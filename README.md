# Herramientas

## Extenciones de Chrome
• [ColorZilla](https://chrome.google.com/webstore/detail/colorzilla/bhlhnicpbhignbdhedgjhgdocnmhomnp)

• [Grid Ruler](https://chrome.google.com/webstore/detail/grid-ruler/joadogiaiabhmggdifljlpkclnpfncmj?hl=es)

• [Viewport Resizer](https://chrome.google.com/webstore/detail/viewport-resizer-%E2%80%93-respon/kapnjjcfcncngkadhpmijlkblpibdcgm)

• [JSON Viewer Awesome](https://chrome.google.com/webstore/detail/json-viewer-awesome/iemadiahhbebdklepanmkjenfdebfpfe)

• [Wappalyzer](https://chrome.google.com/webstore/detail/wappalyzer/gppongmhjkpfnbhagpmjfkannfbllamg?hl=es)

## Angular
• [AngularFire](https://github.com/angular/angularfire)

• [ng2-charts](https://valor-software.com/ng2-charts/)

• [Angular Material](https://material.angular.io/)

• [Angular Tips](https://auth0.com/blog/angular-9s-best-hidden-feature-strict-template-checking/)

• [Angular Bad Practices](https://indepth.dev/angular-bad-practices-revisited/)

• [Angular / Cordova](https://medium.com/@nacojohn/convert-your-angular-project-to-mobile-app-using-cordova-f0384a7711a6)

• [Open-Source Angular Components](https://www.creative-tim.com/blog/web-design/free-open-source-angular-components/)

• [ngx-bootstrap](https://valor-software.com/ngx-bootstrap/#/pagination)

## Colores
• [uigradients](https://uigradients.com/#Socialive)

• [Color Hunt](https://colorhunt.co/palettes)

• [Flatuicolors](http://flatuicolors.com)

• [getUiColors](http://getuicolors.com)

• [Variaciones de Colores](http://www.0to255.com)

• [Adobe Color CC](https://color.adobe.com/create/color-wheel/)

• [Paletton](http://paletton.com)

• [Introducción a Teoría de Color para Diseñadores Web](https://webdesign.tutsplus.com/articles/an-introduction-to-color-theory-for-web-designers--webdesign-1437)

• [MaterialCSS](http://materializecss.com/color.html)

• [Coolors.co](https://coolors.co/app)

• [ColorHexa](https://www.colorhexa.com)

## HTML
• [Pixeden](https://www.pixeden.com/free-design-web-resources)

• [W3School](http://www.w3schools.com/html)

• [Mozilla Developer Network](https://developer.mozilla.org/es/docs/Web/HTML)

• [W3C validator](https://validator.w3.org)

• [Caracteres especiales en HTML](https://css-tricks.com/snippets/html/glyphs/)

## Iconos
• [Font Awesome](https://fontawesome.com/)

• [Material Icons](https://material.io/resources/icons/?style=baseline)

• [Iconmonstr](https://iconmonstr.com/)

• [Iconos8](https://iconos8.es/)

• [Iconfinder](https://www.iconfinder.com/)

• [The Noun Project](https://thenounproject.com/)

• [Feather Icons](https://feathericons.com/)

## Paginas
• [Convert JSON](https://quicktype.io/)

## JavaScript
• [Chart.js](https://www.chartjs.org/)

• [Momentjs](https://momentjs.com/)

• [Hammer.js](https://hammerjs.github.io/)

• [Immerjs](https://github.com/immerjs/immer)

• [Draggable](https://shopify.github.io/draggable/)

• [Axios](https://github.com/axios/axios)

• [Redux](https://redux.js.org/introduction/getting-started)

• [CodeMirror](https://codemirror.net/)

• [Video.js](https://videojs.com/)

• [Sweet Alert 2](https://sweetalert2.github.io/)

## Articulos
• [10 Tips for Writing Clean and Scalable JavaScript](https://icodemag.com/10-tips-for-writing-clean-and-scalable-javascript/)

• [13 Things You Should Know Before You Enter In Web Development](https://www.geeksforgeeks.org/13-things-you-should-know-before-you-enter-in-web-development/)

• [7 really good reasons not to use TypeScript](https://medium.com/javascript-in-plain-english/7-really-good-reasons-not-to-use-typescript-166af597c466)

## Tipografía
• [Google Fonts](https://www.google.com/fonts)

• [FontSquirrel](http://www.fontsquirrel.com)

• [Lista de Mejores Fuentes para Diseñadores](http://www.creativebloq.com/graphic-design-tips/best-free-fonts-for-designers-1233380)

• [Combinaciones de Fuentes](http://fontpair.co)

## Imágenes
• [pikwizard](https://pikwizard.com/)

• [theStocks](http://thestocks.im)

• [Pexels](https://www.pexels.com)

• [Unsplash](https://unsplash.com)

• [Foodiesfeed](https://www.foodiesfeed.com)

• [Imcreator Free](http://www.imcreator.com/free)

• [The patter Library](http://thepatternlibrary.com)

• [Subttle Pattern](https://www.toptal.com/designers/subtlepatterns/)

## Vídeos
• [Pikwizad](https://pikwizard.com/video)

• [Mazwai](http://mazwai.com)

• [Pexels video](https://videos.pexels.com)

• [Videvo](https://www.videvo.net/)

• [Clip Canvas - Free Clips](https://www.clipcanvas.com/free-footage)

• [Vidsplay](https://www.vidsplay.com)

• [Videzzy](https://www.videezy.com/)

• [Free HD Footage](http://www.free-hd-footage.com)

## SVG
• [Vector Logo](https://worldvectorlogo.com/)

## UX

• [Curso Rápido UX en 31 Días](http://thehipperelement.com/post/75476711614/ux-crash-course-31-fundamentals)

• [Trucos de UI](http://www.goodui.org)

• [Más de 100 recursos sobre Diseño de UI](https://github.com/tipoqueno/awesome-ui)

## CSS
• [css-separator-generator](https://wweb.dev/resources/css-separator-generator)

• [fancy corners with CSS](https://blog.logrocket.com/how-to-create-fancy-corners-in-css/)

• [Top 25 CSS Buttons](https://webdeasy.de/en/top-css-buttons-en/)

• [Snippets](https://tympanus.net/codrops/css_reference/)

• [Selectores de CSS que debes memorizar](https://code.tutsplus.com/tutorials/the-30-css-selectors-you-must-memorize--net-16048)

• [Animaciones](https://daneden.github.io/animate.css/)

• [Patrones](https://freebiesupply.com/blog/css-background-patterns/)

• [SKELETON.CSS](http://getskeleton.com/)

• [Normalize.css](https://necolas.github.io/normalize.css/)

## Inspiración
• [Site Inspire](https://www.siteinspire.com)

• [Dribble](https://dribbble.com)

• [behance](https://www.behance.net)

• [One Page Love](https://onepagelove.com)

• [Admire the web](http://www.admiretheweb.com)

• [Flatdsgn](http://flatdsgn.com)

• [Colección de elementos de Diseño](http://calltoidea.com)

• [Más recursos de desarrolladores frontEnd](https://github.com/dypsilon/frontend-dev-bookmarks)

• [oozled](http://oozled.com)

• [Awwwards](https://www.awwwards.com)

## Desarrollo
• [Caracterisiticas de Google Chrome que no estas usando](https://www.labnol.org/software/chrome-dev-tools-tutorial/28131/)

• [El proceso de Diseño web](http://www.newdesigngroup.ca/blog/web-design-process-infographic/)

• [Usuario Aleatorio](https://randomuser.me)

## Optimizando Nuestro Sitio Web
• [Real Favicon Generator](https://realfavicongenerator.net)

• [Favicon Generator](https://www.favicon-generator.org)

• [Minimize Image](http://optimizilla.com)

• [Compress CSS](http://www.minifycss.com/css-compressor/)

• [Compress Javascript](http://www.minifyjavascript.com)

• [Vista previa en multiples dispositivos](http://www.responsinator.com)

• [Google Analytics](https://www.google.com/analytics/)

## SEO
• [WooRank](https://www.woorank.com)

• [SEO](http://webdesign.tutsplus.com/articles/a-web-designers-seo-checklist-including-portable-formats--webdesign-)

## Templates
• [Graphberry](https://www.graphberry.com/category/free-html-web-templates)

## Diagramas
• [Draw.io](https://www.draw.io/)

## Compartir Codigo
• [Stackblitz](https://stackblitz.com/)

• [Codepen](https://codepen.io/)




